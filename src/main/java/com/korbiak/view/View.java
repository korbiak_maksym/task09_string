package com.korbiak.view;

import com.korbiak.Application;
import com.korbiak.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {

    private Controller controller;
    private Logger logger;

    public View() {
        this.controller = new Controller();
        logger = LogManager.getLogger(Application.class);
    }


}
